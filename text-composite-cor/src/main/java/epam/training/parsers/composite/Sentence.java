package epam.training.parsers.composite;

import java.util.List;

public class Sentence extends Composite {
    public Sentence() {
    }

    public Sentence(List<Word> words) {
        words.forEach(this::add);
    }
}
