package epam.training.parsers.composite;

import java.util.List;

public class Text extends Composite {
    public Text() {
    }

    public Text(List<Paragraph> paragraphs) {
        paragraphs.forEach(this::add);
    }
}
