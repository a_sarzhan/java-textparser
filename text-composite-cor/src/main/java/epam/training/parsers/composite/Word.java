package epam.training.parsers.composite;

import java.util.List;

public class Word extends Composite {
    public Word() {
    }

    public Word(List<Letter> letters) {
        letters.forEach(this::add);
    }

    @Override
    public String getElement() {
        return " ";
    }
}
