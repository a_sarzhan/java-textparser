package epam.training.parsers.composite;

import java.util.List;

public class Paragraph extends Composite {
    public Paragraph() {
    }

    public Paragraph(List<Sentence> sentences) {
        sentences.forEach(this::add);
    }

    @Override
    public String getElement() {
        return "\n\t";
    }
}
