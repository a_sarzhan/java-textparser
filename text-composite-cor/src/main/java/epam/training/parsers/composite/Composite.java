package epam.training.parsers.composite;

import java.util.ArrayList;
import java.util.List;

public abstract class Composite {
    private final List<Composite> components;

    Composite() {
        components = new ArrayList<>();
    }

    public int count() {
        return components.size();
    }

    public void add(Composite component) {
        components.add(component);
    }

    public List<Composite> getComponents() {
        return components;
    }

    public String getElement() {
        return "";
    }
}
