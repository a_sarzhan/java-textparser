package epam.training.parsers.composite;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Letter extends Composite {
    private static final Logger logger = LogManager.getLogger(Letter.class);
    private char character;

    public Letter(char character) {
        this.character = character;
    }

    @Override
    public void add(Composite component) {
        logger.error("Composite Object [Letter] can not contain inner composite objects!");
        throw new UnsupportedOperationException();
    }

    @Override
    public String getElement() {
        return Character.toString(character);
    }
}
