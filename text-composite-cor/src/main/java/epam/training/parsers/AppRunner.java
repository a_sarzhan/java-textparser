package epam.training.parsers;

import epam.training.parsers.actions.TextReconstructor;
import epam.training.parsers.actions.TextSorter;
import epam.training.parsers.composite.Text;
import epam.training.parsers.parsers.ParserManager;

public class AppRunner {
    public static void main(String[] args) {
        ParserManager manager = ParserManager.getParserInstance();
        manager.startParsing("text.txt");
        Text textObject = manager.getTextObject();

        System.out.println("-------------------------------------------------------- \n");
        TextReconstructor.printReconstructedTextToConsole(textObject);

        System.out.println("-------------------------------------------------------- \n");
        TextSorter.sortParagraphsBySentenceAmount(textObject);

        System.out.println("-------------------------------------------------------- \n");
        TextSorter.sortSentencesInParagraphsByWordAmount(textObject);

        System.out.println("-------------------------------------------------------- \n");
        TextSorter.sortWordsInSentencesByWordLength(textObject);

        System.out.println("-------------------------------------------------------- \n");
        TextReconstructor.printReconstructedTextToConsole(textObject);
    }
}
