package epam.training.parsers.parsers;

import epam.training.parsers.composite.Sentence;
import epam.training.parsers.composite.Word;

public class WordParser extends TextParser {
    WordParser(TextParser parser) {
        super(parser);
    }

    @Override
    public void parse(String text) {
        Sentence sentence = (Sentence) superComposite;
        for (String word : text.split("\\s+")) {
            superComposite = new Word();
            sentence.add(superComposite);
            super.parse(word);
        }
    }
}
