package epam.training.parsers.parsers;

import epam.training.parsers.composite.Paragraph;
import epam.training.parsers.composite.Text;

public class ParagraphParser extends TextParser {
    ParagraphParser(TextParser parser) {
        super(parser);
    }

    @Override
    public void parse(String text) {
        parsedCompositeText = new Text();
        for (String paragraph : text.split("\n\\s+")) {
            superComposite = new Paragraph();
            parsedCompositeText.add(superComposite);
            super.parse(paragraph);
        }
    }
}
