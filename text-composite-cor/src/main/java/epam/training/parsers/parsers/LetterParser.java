package epam.training.parsers.parsers;

import epam.training.parsers.composite.Letter;
import epam.training.parsers.composite.Word;

public class LetterParser extends TextParser {
    LetterParser(TextParser parser) {
        super(parser);
    }

    @Override
    public void parse(String text) {
        Word word = (Word) superComposite;
        for (char character : text.toCharArray()) {
            word.add(new Letter(character));
        }
    }
}
