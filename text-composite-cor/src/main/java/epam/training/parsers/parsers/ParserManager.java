package epam.training.parsers.parsers;

import epam.training.parsers.composite.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

public class ParserManager {
    private static final Logger logger = LogManager.getLogger(ParserManager.class);
    private final TextParser parser;
    private static final ParserManager PARSER_INSTANCE = new ParserManager();
    private Text textObject;

    private ParserManager() {
        this.parser = new ParagraphParser(new SentenceParser(new WordParser(new LetterParser(null))));
    }

    public static ParserManager getParserInstance() {
        return PARSER_INSTANCE;
    }

    public void startParsing(String fileName) {
        try (InputStream inputStream = Objects.requireNonNull(getClass().getClassLoader().getResource(fileName)).openStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            StringBuilder stringBuilder = new StringBuilder();
            String text;
            while ((text = reader.readLine()) != null) {
                stringBuilder.append(text).append("\n");
            }
            if (!stringBuilder.toString().isEmpty()) {
                logger.info("Parsing file content [file name = " + fileName + "] has been started!");
                parser.parse(stringBuilder.toString().trim());
                this.textObject = parser.parsedCompositeText;
                logger.info("Parsing file content [file name = " + fileName + "] has been finished! \n" +
                        "Composite text object(holding text file content) has been created!");
            } else {
                logger.error("File [" + fileName + "] is empty. Parsing file is Failed!");
            }
        } catch (IOException e) {
            logger.error("Reading file content [file name = " + fileName + "] is failed. Details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public Text getTextObject() {
        return textObject;
    }
}
