package epam.training.parsers.parsers;

import epam.training.parsers.composite.Composite;
import epam.training.parsers.composite.Text;

public abstract class TextParser {
    private TextParser nextParser;
    Composite superComposite;
    Text parsedCompositeText;

    TextParser(TextParser parser) {
        this.nextParser = parser;
    }

    public void parse(String text) {
        if (nextParser != null) {
            nextParser.parsedCompositeText = parsedCompositeText;
            nextParser.superComposite = superComposite;
            nextParser.parse(text);
        }
    }
}
