package epam.training.parsers.parsers;

import epam.training.parsers.composite.Paragraph;
import epam.training.parsers.composite.Sentence;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser extends TextParser {
    SentenceParser(TextParser parser) {
        super(parser);
    }

    @Override
    public void parse(String text) {
        Paragraph paragraph = (Paragraph) superComposite;
        Pattern pattern = Pattern.compile("(.|\n)+?(\\?|!|\\.{3}|\\.)");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            String sentence = text.substring(matcher.start(), matcher.end()).trim();
            superComposite = new Sentence();
            paragraph.add(superComposite);
            super.parse(sentence);
        }
    }
}
