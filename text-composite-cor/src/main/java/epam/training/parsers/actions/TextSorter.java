package epam.training.parsers.actions;

import epam.training.parsers.composite.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class TextSorter {
    private static final Logger logger = LogManager.getLogger(TextSorter.class);

    public static void sortParagraphsBySentenceAmount(Text textObject) {
        List<Paragraph> textParagraphs = (List<Paragraph>) (List<?>) textObject.getComponents();
        List<Paragraph> paragraphList = new ArrayList<>(textParagraphs);
        paragraphList.sort((Composite o1, Composite o2) -> o1.count() - o2.count());
        Text sortedTextObject = new Text(paragraphList);
        logger.info("Text paragraphs are sorted by sentences amount in ascending order.");
        TextReconstructor.printReconstructedTextToConsole(sortedTextObject);
    }

    public static void sortSentencesInParagraphsByWordAmount(Text textObject) {
        List<Paragraph> textParagraphs = (List<Paragraph>) (List<?>) textObject.getComponents();
        List<Paragraph> paragraphList = new ArrayList<>();
        List<Sentence> sentenceList;
        for (Paragraph paragraph : textParagraphs) {
            List<Sentence> textSentences = (List<Sentence>) (List<?>) paragraph.getComponents();
            sentenceList = new ArrayList<>(textSentences);
            sentenceList.sort((Composite o1, Composite o2) -> o1.count() - o2.count());
            paragraphList.add(new Paragraph(sentenceList));
        }
        Text sortedTextObject = new Text(paragraphList);
        logger.info("Sentences of paragraphs are sorted by words amount in ascending order.");
        TextReconstructor.printReconstructedTextToConsole(sortedTextObject);
    }

    public static void sortWordsInSentencesByWordLength(Text textObject) {
        List<Paragraph> textParagraphs = (List<Paragraph>) (List<?>) textObject.getComponents();
        List<Paragraph> paragraphList = new ArrayList<>();
        List<Sentence> sentenceList;
        List<Word> wordList;
        for (Paragraph paragraph : textParagraphs) {
            List<Sentence> textSentences = (List<Sentence>) (List<?>) paragraph.getComponents();
            sentenceList = new ArrayList<>();
            for (Sentence sentence : textSentences) {
                List<Word> textWords = (List<Word>) (List<?>) sentence.getComponents();
                wordList = new ArrayList<>(textWords);
                wordList.sort((Composite o1, Composite o2) -> o1.count() - o2.count());
                sentenceList.add(new Sentence(wordList));
            }
            paragraphList.add(new Paragraph(sentenceList));
        }
        Text sortedTextObject = new Text(paragraphList);
        logger.info("Words in sentences are sorted by word length in ascending order.");
        TextReconstructor.printReconstructedTextToConsole(sortedTextObject);
    }
}
