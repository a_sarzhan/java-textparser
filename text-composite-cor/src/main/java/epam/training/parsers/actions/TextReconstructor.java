package epam.training.parsers.actions;

import epam.training.parsers.composite.Composite;
import epam.training.parsers.composite.Paragraph;
import epam.training.parsers.composite.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TextReconstructor {
    private static final Logger logger = LogManager.getLogger(TextReconstructor.class);
    private static StringBuilder stringBuilder;
    private static final int MAX_WIDTH = 100;

    private static String getReconstructedTextOfFileFromCompositeObject(Composite textComposite) {
        stringBuilder = new StringBuilder();
        fillStringBuilderWithElementsOfTextComposite(textComposite);
        logger.info("Text content from Composite Object successfully formatted!");
        if (stringBuilder.length() != 0) {
            stringBuilder.delete(stringBuilder.lastIndexOf("\n\t"), stringBuilder.length());
        }
        logger.info("Text content from Composite Text Object has been successfully reconstructed!");
        return stringBuilder.toString();
    }

    private static void fillStringBuilderWithElementsOfTextComposite(Composite textComposite) {
        for (Composite composite : textComposite.getComponents()) {
            fillStringBuilderWithElementsOfTextComposite(composite);
        }
        if (textComposite instanceof Paragraph) {
            formatParagraphInStringBuilder();
        }
        stringBuilder.append(textComposite.getElement());
    }

    private static void formatParagraphInStringBuilder() {
        int beginIndex = stringBuilder.lastIndexOf("\n\t") + 1;
        if (beginIndex == 0) {
            stringBuilder.insert(0, "\t");
        }
        int lastIndex = MAX_WIDTH + beginIndex;
        while (lastIndex < stringBuilder.length() - 1) {
            int replaceIndex = stringBuilder.lastIndexOf(" ", lastIndex);
            stringBuilder.replace(replaceIndex, replaceIndex + 1, "\n");
            lastIndex += MAX_WIDTH;
        }
    }

    public static void writeReconstructedTextToFile(Text textObject) {
        String textContent = getReconstructedTextOfFileFromCompositeObject(textObject);
        try (FileWriter fileWriter = new FileWriter("reconstructedText.txt");
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            bufferedWriter.write(textContent);
        } catch (IOException e) {
            logger.error("Error occurred while writing text content to File. Details: " + e.getMessage());
            e.printStackTrace();
        }
        logger.info("Reconstructed text content has been successfully written to file [reconstructedText.txt]");
    }

    public static void printReconstructedTextToConsole(Text textObject) {
        String textContent = getReconstructedTextOfFileFromCompositeObject(textObject);
        System.out.println(textContent);
    }
}
